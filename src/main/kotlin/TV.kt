class TV (val brand: String, val model: String, val diagonal: Double) {
    var isTurnedOn = false
    var currentVolume = 0
    var currentChannel = 0

    private val channelList = Channels.getRandomChannels()

    fun printChannels(){
        println(channelList)
    }

    fun printTVInfo(){
        println("Марка - $brand, модель - $model, диагональ - $diagonal")
    }

    fun printTVStatus(){
        println("ТВ включен - $isTurnedOn, канал - $currentChannel")
    }

    fun turnOnOff(): Boolean {
        // телик выключен, включаем, если это первое включение, то он включится на нулевом канале,
        // а если до выключения были действия, то включится на запомненном (последний до выключения)
        if (isTurnedOn == false) {
            isTurnedOn = true
            currentChannel = currentChannel
            println("TV is on, currentChannel - $currentChannel")
        }
        // телик включен, выключаем, запоминаем канал
        else {
            isTurnedOn = false
            currentChannel = currentChannel
            println("TV is off, currentChannel - $currentChannel")
        }
        return isTurnedOn
    }

    fun volumeUp(volume: Int): Int{
        if (volume in 1..maxVolume && (currentVolume + volume) < 100) currentVolume += volume
        else println("Такое изменение громкости невозможно")
        println("Текущая громкость - $currentVolume")
        return currentVolume
    }

    fun volumeDown(volume: Int): Int{
        if (volume in 1..maxVolume && (currentVolume - volume) > 0) currentVolume -= volume
        else if (volume in 1..maxVolume) currentVolume = 0
        else println("Такое изменение громкости невозможно")
        println("Текущая громкость - $currentVolume")
        return currentVolume
    }

    fun switchToChannelNumber(channel: Int){
        // если телик выключен
        if (!isTurnedOn) {
            // если номер в списке каналов, то окей, включается канал
            if (channel in channelList.keys && channel > 0) {
                isTurnedOn = true
                currentChannel = channelList.keys.elementAt(channel)
                println("TV is on, isturnedON - $isTurnedOn, канал - $currentChannel")
            // если номер за рамками списка каналов или отрицательный, то включаем канал 0 и пишем об этом
            } else {
                currentChannel = 0
                println("Номер канала не в списке каналов, включен канал 0")
            }
        // если телик включен и хотим включить номер канала, входящий в список, то переходим на номер канала
        } else {
            // и хотим включить номер канала, входящий в список, то переходим на номер канала
            if (channel in channelList.keys && channel > 0) {
                currentChannel = channelList.keys.elementAt(channel)
                println("TV is on, isturnedON - $isTurnedOn, канал - $currentChannel")
            // если номер за рамками списка каналов или отрицательный, то включаем канал 0 и пишем об этом
            } else {
                currentChannel = 0
                println("Номер канала не в списке каналов, включен канал 0")
            }
        }
    }

    fun channelPlus(){
        // если телик выключен, то включаемся на канале, который был до выключения
        if (!isTurnedOn) {
            isTurnedOn = true
            currentChannel = currentChannel
            println("TV is on, isturnedON - $isTurnedOn, канал - $currentChannel")
        // если телик включен
        } else {
            // на последнем канале из списка, то переходим на первый из списка.
            if (currentChannel == channelList.keys.last()) {
                currentChannel = channelList.keys.first()
                println("TV is on, isturnedON - $isTurnedOn, канал - $currentChannel")
            // если мы не на последнем канале, то переключаемся на плюс 1.
            } else {
                currentChannel += 1
            println("TV is on, isturnedON - $isTurnedOn, канал - $currentChannel")
            }
        }
    }

    fun channelMinus(){
        // если телик выключен, то включаемся на канале, который был до выключения
        if (!isTurnedOn) {
            isTurnedOn = true
            currentChannel = currentChannel
            println("TV is on, isturnedON - $isTurnedOn, канал - $currentChannel")
        // если телик включен
        } else {
            // на первом канале из списка, то переходим на последний из списка.
            if (currentChannel == channelList.keys.first()) {
                currentChannel = channelList.keys.last()
                println("TV is on, isturnedON - $isTurnedOn, канал - $currentChannel")
            // если мы не на последнем канале, то переключаемся на плюс 1.
            } else {
                currentChannel -= 1
                println("TV is on, isturnedON - $isTurnedOn, канал - $currentChannel")
            }
        }
    }

    companion object {
        const val maxVolume = 100
    }
}