import TV.Companion.maxVolume

fun main() {
    val firstTV = TV("Samsung", "FD40s", 39.5)
    val secondTV = TV("LG", "SE50s", 49.0)

    // блок инфо
    firstTV.printTVInfo()
    firstTV.printChannels()
    secondTV.printTVInfo()
    secondTV.printChannels()
    println("----------")

    // здесь и далее всё на одном экземпляре ТВ, упор на работу функций.
    // блок вкл\выкл
    firstTV.turnOnOff()
    println(firstTV.isTurnedOn)
    firstTV.turnOnOff()
    println(firstTV.isTurnedOn)
    firstTV.turnOnOff()
    println(firstTV.isTurnedOn)
    println("----------")

    //блок с громкостью
    println(firstTV.currentVolume)
    firstTV.volumeUp(20)
    firstTV.volumeUp(20)
    firstTV.volumeUp(120)
    firstTV.volumeUp(20)
    firstTV.volumeDown(40)
    firstTV.volumeDown(-40)

    // блок с громкостью через цикл
    for (i in 0..maxVolume step 10){
        firstTV.volumeUp(10)
    }
}