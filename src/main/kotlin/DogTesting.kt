fun main() {
    val dog1 = Dog("Tuzik", 2)

    dog1.dogInfo()
    println("${dog1.nick} has ${Dog.legsNumber} legs.")
    dog1.move()
    dog1.eat("Meat")
    dog1.gettingOlder(3)
    dog1.dogInfo()
}