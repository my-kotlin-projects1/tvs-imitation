class Dog (val nick: String, var age: Int, var isHungry: Boolean = true){

    init {
        println("${nick.uppercase()} says \"Гав!\"")
    }

    fun dogInfo() = println("Dog's nick is $nick, it's age is $age, it is hungry - $isHungry.")

    fun move() = println("$nick moves.")

    fun eat(food: String): Boolean{
        println("$nick eats $food.")
        isHungry = false
        return isHungry
    }

    fun gettingOlder(age: Int): Int {
        this.age += age
        println("${this.nick} become elder. Now its age is ${this.age}.")
        return this.age
    }

    companion object SomeInfo{
        const val legsNumber = 4
        fun eat(food: String){
        }
    }
}