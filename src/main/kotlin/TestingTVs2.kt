import kotlin.concurrent.thread

fun main() {
    val firstTV = TV("Samsung", "FD40s", 39.5)
    val secondTV = TV("LG", "SE50s", 49.0)

    // блок инфо
    firstTV.printTVInfo()
    firstTV.printChannels()
    secondTV.printTVInfo()
    secondTV.printChannels()
    println("----------")
    Thread.sleep(1000)

    // здесь и далее всё на одном экземпляре ТВ, упор на работу функций
    // нажатие на кнопку номера канала
    firstTV.switchToChannelNumber(3) // включает телик на указанном канале
    firstTV.switchToChannelNumber(11) // на включенном канале не переключается на канал не в диапазоне
    firstTV.switchToChannelNumber(2) // на включенном телике переключает на указанный канал
    println("----------")
    Thread.sleep(2000)

    // нажатие на кнопку Channel Plus (и с Channel Minus аналогично)
    firstTV.channelPlus() // телик включен с предыдущего блока, кнопка переключает на следующий канал
    firstTV.turnOnOff() // выключили телик
    firstTV.channelPlus() // телик включается на том канале, который был до выключения
    println("----------")
    Thread.sleep(2000)

    // нажатие на кнопку Channel Plus через цикл, здесь же и проверка перехода через ноль
    for (i in 0..10){
        firstTV.channelPlus()
        Thread.sleep(1000)
    }
}