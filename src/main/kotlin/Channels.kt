import kotlin.random.Random

object Channels {
    private val channelList = listOf<String>(
        "Первый канал",
        "Культура",
        "Discovery",
        "NatGeoWild",
        "Звезда",
        "ТНТ",
        "НТВ",
        "РенТВ",
        "Comedy TV",
        "Disney",
        "2x2",
        "Матч ТВ",
        "Домашний",
        "Россия 24",
        "ТВ Центр - Урал"
    )

    fun getRandomChannels(): Map<Int, String> {
     val shuffledList = channelList.shuffled().subList(0, Random.nextInt(1, channelList.size - 1))
     val resultMap = mutableMapOf<Int, String>()
     for (i in shuffledList.indices){
         resultMap[i] = shuffledList[i]
     }
     return resultMap
    }
}